package com.xiayitriao.com.xiayitiao.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
//@RestController注解是@ResponseBody和@Controller的集合体，使用@RestController注解会默认返回数据，而不会请求到页面。
@Controller
public class MainController {

    @RequestMapping("/")
    public String thymeleaf(ModelMap map) {
        // 加入一个属性，用来在模板中读取
        map.addAttribute("host","易小熊");
        // return模板文件的名称，对应src/main/resources/templates/thymeleaf.html
        return "thymeleaf";
    }

    @RequestMapping("/hello")
    public String sayHello() {
        return "hello world";
    }
}
